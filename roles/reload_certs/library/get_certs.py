#!/usr/bin/python3
import json
from sys import argv, stderr
import os


def parse_args() -> dict:
    with open(argv[1]) as f:
        raw_args = []
        line = ""
        for x in f.read().split(' '):
            line += x
            if line.count("'") % 2 == 0:
                raw_args.append(line)
                line = ""

    args = {}
    for raw_arg in raw_args:
        if not raw_arg or raw_arg.startswith("_ansible_"):
            continue
        args[raw_arg.split('=')[0]] = raw_arg.split('=')[1]

    if not "path" in args:
        print(json.dumps({'failed': True, 'reason': 'path not specified'}))
        exit(0)

    return {"path": args["path"]}


def process_certfile(path: str) -> list:
    results = []
    buff = ""
    with open(path) as f:
        for line in f.readlines():
            if line == "\n":
                results.append(buff)
                buff = ''
            else:
                buff += line
        if buff:
            results.append(buff)
    return results


def process_keyfile(path: str) -> str:
    with open(path) as f:
        return f.read()


def main():
    base_path = parse_args()["path"]

    try:
        for f in os.listdir(base_path):
            if f.endswith(".crt"):
                cetrificates = process_certfile(base_path + "/" + f)
            elif f.endswith(".key"):
                key = process_keyfile(base_path + "/" + f)
    except FileNotFoundError:
        print(json.dumps({'failed': True, 'reason': 'path does not exist'}))
        exit(0)

    print(json.dumps({"ansible_module_results": {
          "certs": cetrificates, "key": key}}))


main()
